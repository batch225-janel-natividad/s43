let posts = []; //storage
let count = 1; //id

//add post data.

document.querySelector('#form-add-post').addEventListener('submit',(e) => {
    e.preventDefault();
    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        body: document.querySelector('#txt-body').value
    });

    count++;

    console.log(posts);
    showPosts(posts);
    alert('Successful added')
})

//Show Posts

const showPosts = (posts) => {
    let postEntries = '';

    posts.forEach((post) => {
        postEntries += 
        `<div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title} </h3>
        <p id="post-body-${post.id}">${post.body}</p>
        </div>
        <button onclick="editPost(${post.id})">Edit</button>
        <button onclick="deletePost(${post.id})">Delete</button>`
    });

    document.querySelector('#div-post-entries').innerHTML = postEntries;
}


// Edit post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;

}

// Update post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
        e.preventDefault();

        for (let i = 0; i< posts.length; i++) {
        // The Value posts[i].id is a Number while document.querySelector('#txt-edit-id).value is String
        // Therefore, it is necessary to convert the Number to a String first.
            if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
                posts[i].title = document.querySelector('#txt-edit-title').value;
                posts[i].body = document.querySelector('#txt-edit-body').value;
                showPosts(posts);
                alert('Successfully updated!');
            }
            

            //break;
        }
    })
//Edit Post
// const editPost = (id) => {
//     const post = posts.find(post => post.id === id);

//     document.querySelector(`#txt-title`).value = post.title;
//     document.querySelector(`#txt-body`).value = post.body;

//     document.querySelector('#form-add-post').addEventListener('submit', (e) => {
//         e.preventDefault();
//         post.title = document.querySelector(`#txt-title`).value;
//         post.body = document.querySelector(`#txt-body`).value;

//         showPosts(posts);
//         alert('Successful updated');
//     });
// }

// Delete post

const deletePost = (id) => {
    const postIndex = posts.findIndex(post => post.id === id);
    posts.splice(postIndex, 1);
    showPosts(posts);
    alert('Successful deleted');
}

// // Show Posts

// const showPosts2 = (posts) => {
//     let postEntries = '';

//     posts.forEach((post) => {
//         postEntries += 
//         `<div id="post-${post.id}">
//         <h3 id="post-title-${post.id}">${post.title} </h3>
//         <p id="post-body-${post.id}">${post.body}</p>
//         <button onclick="editPost(${post.id})">Edit</button>
//         <button onclick="deletePost(${post.id})">Delete</button>
//         </div>`
//     });

//     document.querySelector('#div-post-entries').innerHTML = postEntries;
// }